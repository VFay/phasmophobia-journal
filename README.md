# PhasmophobiaJournal

Side project for the Phasmophobia cheat sheet. Information about ghost behavior is taken [from](https://docs.google.com/spreadsheets/d/1Cr9J7lUqYeUB05CoDmUq19as-_gf-RT6kng7uJCK-4g/edit?pli=1#gid=0)

To view project, please follow this [url](https://phasmophobia-journal-vfay-5237fca615bf323dc9aee4564e6a0d002c33c.gitlab.io/)

Weekly challenges descriptions are taken from [BigMeikLIVE](https://twitch.tv/bigmeiklive)
