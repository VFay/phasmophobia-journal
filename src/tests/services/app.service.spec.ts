import moment from "moment";
import { AppService } from "../../app/services/app.service"

describe('AppService', () => {
    let service: AppService;

    beforeEach(() => {
        service = new AppService();
    })

    it('AppService.getChallenges should update on Monday UTC', () => {
        let date = moment.utc("2024-01-01 00:00").toDate();
        let challenges = service.getChallenges(date);

        expect(challenges[0].name).toEqual("Speed Demons");

        date = moment.utc("2024-01-08 00:00").toDate();
        challenges = service.getChallenges(date);

        expect(challenges[0].name).toEqual("Detectives only");
    });

    it('AppService.getChallenges should loop over to first challenge', () => {
        let date = moment.utc("2024-07-01 00:00").toDate();
        let challenges = service.getChallenges(date);

        expect(challenges[0].name).toEqual("Speed Demons");
    });

    it('AppService.getChallenges should loop over on leap year', () => {
        let date = moment.utc("2024-12-23 00:00").toDate();
        let challenges = service.getChallenges(date);

        expect(challenges[0].name).toEqual("Lights out!");

        date = moment.utc("2024-12-30 00:00").toDate();
        challenges = service.getChallenges(date);

        expect(challenges[0].name).toEqual("Speed Demons");

        date = moment.utc("2025-01-01 00:00").toDate();
        challenges = service.getChallenges(date);

        expect(challenges[0].name).toEqual("Speed Demons");
    });

    it('AppService.getChallenges should return valid challenge on week 35', () => {
        let date = moment.utc("2024-08-26 00:00").toDate();
        let challenges = service.getChallenges(date);

        expect(challenges[0].name).toEqual("Gotta Go Fast!");
    });
})
