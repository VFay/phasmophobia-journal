import { Component, Input } from '@angular/core';
import { Challenge } from '../../models/challenge';

@Component({
    selector: 'challenge',
    templateUrl: './challenge.html',
    styleUrls: ['./challenge.scss']
})
export class ChallengeComponent {
    @Input() challenge: Challenge | undefined;
}
