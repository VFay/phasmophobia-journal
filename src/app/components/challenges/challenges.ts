import { Component, OnInit } from '@angular/core';

import { AppService } from '../../services/app.service';
import { Challenge } from '../../models/challenge';
import moment from 'moment';

@Component({
    selector: 'challenges',
    templateUrl: './challenges.html'
})
export class ChallengesComponent implements OnInit {
    challenges: Challenge[] = []

    constructor(private appService: AppService) {
    }

    ngOnInit(): void {
        this.challenges = this.appService.getChallenges(moment().utc().toDate());
    }
}
