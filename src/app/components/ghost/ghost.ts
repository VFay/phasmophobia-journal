import { Component, Input } from '@angular/core';
import { Ghost } from '../../models/ghost';

@Component({
    selector: 'ghost',
    templateUrl: './ghost.html'
})
export class GhostComponent {
    @Input() ghost: Ghost | undefined;
}
