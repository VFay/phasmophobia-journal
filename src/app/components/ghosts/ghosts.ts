import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

import { AppService } from '../../services/app.service';
import { Ghost } from '../../models/ghost';
import { Evidence, getAllEvidenceOptions } from '../../models/evidence';
import { MenuItemStyles } from '../../models/constants';

@Component({
    selector: 'ghosts',
    templateUrl: './ghosts.html'
})
export class GhostsComponent implements OnInit {
    ghosts: Ghost[] = [];
    menuGhosts: MenuItem[] = [];
    evidenceOptions: Evidence[] = [];

    selectedGhosts: Ghost[] = [];
    selectedEvidence: Evidence[] = [];

    checked = true;

    constructor(private appService: AppService) {
    }

    ngOnInit(): void {
        const ghosts = this.appService.getGhosts();
        this.evidenceOptions = getAllEvidenceOptions();

        this.ghosts = ghosts;
        this.selectedGhosts = ghosts;
        this.menuGhosts = this.getMenuGhostItems(ghosts);
    }

    onEvidenceSelection() {
        this.selectedGhosts = this.ghosts.filter(g => this.selectedEvidence.every(e => g.evidence.includes(e)));
        this.updateSelectedMenuItemBackgroud();
    }

    getMenuGhostItems(ghosts: Ghost[]): MenuItem[] {
        return ghosts.map(g => {
            return {
                id: g.id,
                label: g.name,
                styleClass: MenuItemStyles.Empty,
                command: event => {
                    const selectedGhost = this.menuGhosts.find(g => g.id === event.item?.id)!;

                    this.updateSelectedGhosts(selectedGhost);
                    this.updateSelectedMenuItemBackgroud(selectedGhost);
                }
            }
        });
    }

    updateSelectedGhosts(ghost: MenuItem) {
        this.selectedGhosts = !ghost.styleClass ? [this.ghosts.find(g => g.id === ghost.id)!] : this.ghosts;
    }

    updateSelectedMenuItemBackgroud(ghost?: MenuItem) {
        this.menuGhosts.forEach(m => {
            if (ghost && m.id === ghost.id) {
                m.styleClass = m.styleClass ? MenuItemStyles.Empty : MenuItemStyles.Active;
            } else {
                m.styleClass = MenuItemStyles.Empty;
            }
        });
    }
}
