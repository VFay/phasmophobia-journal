import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',  
  styles: `.menu-container { margin-bottom: 10px; }`
})
export class AppComponent implements OnInit {
    tabs: MenuItem[] | undefined;
    activeItem: MenuItem | undefined;

    ngOnInit(): void {
      this.tabs = [{
        id: 'ghosts',
        label: "Ghosts",
        icon: "gg-ghost-character"
      }, {
        id: 'challenges',
        label: "Challenges",
        icon: "gg-calendar-today"
      }];

      this.activeItem = this.tabs[0];
    }

    onActiveItemChange(event: MenuItem) {
        this.activeItem = event;
    }
}
