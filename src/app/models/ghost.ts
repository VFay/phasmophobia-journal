import { Evidence } from "./evidence";

export interface Ghost {
    id: string,
    name: string,
    evidence: Evidence[],
    jornalDescription: string,
    huntThreshold: string,
    ghostSpeed: string,
    giveAway: string,
    excluder: string,
    hiddenAbility: string,
    notes: string
}