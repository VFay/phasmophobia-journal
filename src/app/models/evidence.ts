export enum Evidence {
    Dots = 'D.O.T.S',
    EMF5 = 'EMF Level 5',
    Ultraviolet = 'Ultraviolet',
    FreezingTemperatures = 'Freezing Temperatures',
    GhostOrb = 'Ghost Orb',
    GhostWriting = 'Ghost Writing',
    SpiritBox = 'Spirit Box'
}

export function getAllEvidenceOptions() {
    return [
        Evidence.Dots, 
        Evidence.EMF5,
        Evidence.FreezingTemperatures, 
        Evidence.GhostOrb, 
        Evidence.GhostWriting, 
        Evidence.SpiritBox, 
        Evidence.Ultraviolet]
}