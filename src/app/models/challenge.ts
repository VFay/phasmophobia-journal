export interface Challenge {
    id: string,
    name: string,
    description: string,
    descriptionUrl?: string,
    map: string,
    start: Date
}