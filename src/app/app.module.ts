import { NgModule, } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputSwitchModule } from 'primeng/inputswitch'
import { CardModule } from 'primeng/card';
import { DividerModule } from 'primeng/divider';
import { ListboxModule } from 'primeng/listbox';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { AppComponent } from './app.component';
import { ButtonModule } from "primeng/button"
import { TabMenuModule } from "primeng/tabmenu"
import { GhostComponent } from './components/ghost/ghost';
import { GhostsComponent } from './components/ghosts/ghosts';
import { ChallengesComponent } from "./components/challenges/challenges"
import { ChallengeComponent } from './components/challenge/challenge'

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DividerModule,
    ListboxModule,
    SelectButtonModule,
    TieredMenuModule,
    CardModule,
    ButtonModule,
    ScrollPanelModule,
    InputSwitchModule,
    TabMenuModule,
    RouterModule.forRoot([])],
    declarations: [AppComponent, GhostComponent, GhostsComponent, ChallengesComponent, ChallengeComponent ],
    bootstrap: [AppComponent],
    providers: []
})

export class AppModule {}