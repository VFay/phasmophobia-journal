import { Injectable } from '@angular/core';
import data from '../../assets/data_eng.json';
import { Ghost } from '../models/ghost';
import { Challenge } from '../models/challenge';
import moment from 'moment';

@Injectable({ providedIn: 'root' })
export class AppService {
    getGhosts() {
        return data.ghosts as Ghost[]
    }

    getChallenges(date: Date) {
        const currentMonday = moment.utc(date).startOf("isoWeek");
        const numberOfWeeksFromStart = currentMonday.diff(moment.utc("2024-01-01 00:00"), "week") + 1;

        const challenges: Challenge[] = Array.from({ length: data.challenges.length })

        for (let i = numberOfWeeksFromStart; i < numberOfWeeksFromStart + data.challenges.length; i++) {
            const start = new Date()
            start.setDate(currentMonday.toDate().getDate() + 7 * (i - numberOfWeeksFromStart))

            // This works because of coincidental correlation between week number and challenge order.
            // "Speed Demons" was a challenge on Jan 1 2024, and it is second challenge in the list.
            // Since collection index starts from zero, taking data.challenges[number_of_current_week] returns correct item.
            // Using '%' operation loops item selection around (e.g. 26 % 26 == 0 -> take first item; 27 % 26 = 1 -> take second item)
            // Counting number of weeks from Jan 1 2024 makes this logic work past December 2024,
            // for non leap years and years that don't start on Monday (e.g. 55 % 26 == 3, etc.).
            challenges[i - numberOfWeeksFromStart] = {
                ...data.challenges[i % (data.challenges.length)],
                start
            }
        }

        return challenges;
    }
}
